resource "aws_subnet" "MySQL_pub_subnet" {
  count             = length(var.pub_subnet_cidr)
  cidr_block        = var.pub_subnet_cidr[count.index]
  vpc_id            = aws_vpc.MySQL_vpc.id
  availability_zone = var.az[count.index]

  tags = {
    #count = length(var.pub_subnet_name)
    Name = var.pub_subnet_name[count.index]
  }
}

resource "aws_subnet" "MySQL_priv_subnet" {
  count             = length(var.priv_subnet_cidr)
  cidr_block        = var.priv_subnet_cidr[count.index]
  vpc_id            = aws_vpc.MySQL_vpc.id
  availability_zone = var.az[count.index]

  tags = {
    #count = length(var.priv_subnet_name)
    Name = var.priv_subnet_name[count.index]
  }
}