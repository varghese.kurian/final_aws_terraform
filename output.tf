output "vpc_ID" {

  value = aws_vpc.MySQL_vpc.id
}

output "Public_Subnet_Id_1" {

  value = aws_subnet.MySQL_pub_subnet[0].id
}

output "Public_Subnet_Id_2" {
  value = aws_subnet.MySQL_pub_subnet[1].id
}

output "Private_SubnetId_1" {
  value = aws_subnet.MySQL_priv_subnet[0].id
}

output "Private_SubnetId_2" {
  value = aws_subnet.MySQL_priv_subnet[1].id
}

output "IGW_ID" {

  value = aws_internet_gateway.MySQL_igw.id
}

output "NAT_ID" {

  value = aws_nat_gateway.MySQL_nat.id
}

output "Pub_Route_Id" {

  value = aws_route_table.MySQL_pub_route.id
}

output "Priv_Route_Id" {

  value = aws_route_table.MySQL_priv_route.id
}

output "bastion_host_id" {
  value = aws_instance.host.id

}

output "node_1_ID" {
  value = aws_instance.node[0].id

}

output "node_2_ID" {
  value = aws_instance.node[1].id

}

output "node_3_ID" {
  value = aws_instance.node_b.id

}

output "bastion_host_sg_ID" {
  value = aws_security_group.bastion_sg.id

}

output "node_sg_ID" {
  value = aws_security_group.priv_sg.id

}

output "bastion_host_Public_IP" {
  value = aws_instance.host.public_ip

}

output "node_1_Private_IP" {
  value = aws_instance.node[0].private_ip

}
output "node_2_Private_IP" {
  value = aws_instance.node[1].private_ip

}
output "node_3_Private_IP" {
  value = aws_instance.node_b.private_ip

}