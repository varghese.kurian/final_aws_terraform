resource "aws_internet_gateway" "MySQL_igw" {
  vpc_id = aws_vpc.MySQL_vpc.id

  tags = {
    Name = var.igw_name
  }
}

resource "aws_eip" "allocating_EIP" {
}

resource "aws_nat_gateway" "MySQL_nat" {

  subnet_id     = aws_subnet.MySQL_pub_subnet[0].id
  allocation_id = aws_eip.allocating_EIP.id

  tags = {
    Name = var.nat_name
  }
}
