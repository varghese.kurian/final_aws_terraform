data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}

resource "aws_instance" "host" {
  ami                         = var.ami_id
  instance_type               = var.inst_type
  subnet_id                   = aws_subnet.MySQL_pub_subnet[0].id
  associate_public_ip_address = "true"
  vpc_security_group_ids      = [aws_security_group.bastion_sg.id]
  key_name                    = var.key

  user_data = <<-EOF
    #!/bin/bash
    sudo apt-get update 
    sudo apt install ansible -y
    sudo apt install git -y
    git clone https://gitlab.com/varghese.kurian/final_ansible_role.git
EOF

  tags = {
    Name = var.host_name
  }
}

resource "aws_instance" "node" {
  ami                    = var.ami_id
  instance_type          = var.inst_type
  count                  = length(var.priv_subnet_cidr)
  subnet_id              = aws_subnet.MySQL_priv_subnet[count.index].id
  vpc_security_group_ids = [aws_security_group.priv_sg.id]
  key_name               = var.key

#   user_data = <<-EOF
#     #!/bin/bash
#     sudo apt-get update
#     sudo apt-get install prometheus-node-exporter -y
#     sudo systemctl start prometheus-node-exporter
#     sudo systemctl enable prometheus-node-exporter
# EOF  

  tags = {
    Name = var.node_name[count.index]
  }
}

resource "aws_instance" "node_b" {
  ami                    = var.ami_id
  instance_type          = var.inst_type
  subnet_id              = aws_subnet.MySQL_priv_subnet[1].id
  vpc_security_group_ids = [aws_security_group.priv_sg.id]
  key_name               = var.key

#   user_data = <<-EOF
#     #!/bin/bash
#     sudo apt-get update
#     sudo apt-get install prometheus-node-exporter -y
#     sudo systemctl start prometheus-node-exporter
#     sudo systemctl enable prometheus-node-exporter
# EOF 

  tags = {
    Name = var.node_name[2]
  }
}


locals {
  ingress_rules = [{
    port        = 22
    description = "SSH Port 22"
    },
    {
      port        = 3306
      description = "Mysql Port"
    },
    {
      port        = 80
      description = "Http & Https"
    },
    {
      port        = 9090
      description = "Promethus port"
    },
    {
      port        = 9100
      description = "Node exporter port"
    },
    {
      port        = 9104
      description = "Mysql exporter port 9104"
      #port of MySQL-exporter
    },
    {
      port        = 9200
      description = "Elastic search port 9200"
      
    },
    {
      port        = 5601
      description = "kibana port"
     
    },
    {
      port        = 5044
      description = "logstash"
    },
    {
      port        = 24224
      description = "Fluentd"
    },

    {
      port        = 3000
      description = "graffana"
  }]
}

resource "aws_security_group" "bastion_sg" {
  name   = "resource_with_dynamic_block_for_host"
  vpc_id = aws_vpc.MySQL_vpc.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  dynamic "ingress" {
    for_each = local.ingress_rules

    content {
      description = ingress.value.description
      from_port   = ingress.value.port
      to_port     = ingress.value.port
      protocol    = "tcp"
      cidr_blocks = ["${chomp(data.http.myip.body)}/32"]
    }

  }

  tags = {
    Name = "AWS security group dynamic block for host"
  }
}

locals {
  ingress_rule = [{
    port        = 22
    description = "SSH Port 22"
    },
    {
      port        = 3306
      description = "Mysql Port"
    },
    {
      port        = 80
      description = "Http & Https"
    },
    {
      port        = 9100
      description = "Node exporter port"
    },
    {
      port        = 5044
      description = "logstash"
    },
    {
      port        = 24224
      description = "Fluentd"
    },
    {
      port        = 9104
      description = "Mysql exporter port 9104"
      #port of MySQL-exporter
    }]
}

resource "aws_security_group" "priv_sg" {
  name   = "resource_with_dynamic_block_for_node"
  vpc_id = aws_vpc.MySQL_vpc.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  dynamic "ingress" {
    for_each = local.ingress_rule

    content {
      description = ingress.value.description
      from_port   = ingress.value.port
      to_port     = ingress.value.port
      protocol    = "tcp"
      cidr_blocks = ["${chomp(aws_instance.host.private_ip)}/32"]
    }

  }

  tags = {
    Name = "AWS security group dynamic block for node"
  }
}
