variable "vpc_cidr" {
  default = "192.168.0.0/16"

}

variable "vpc_name" {
  type    = string
  default = "MySQL-vpc"

}

variable "pub_subnet_cidr" {
  default = ["192.168.0.0/24", "192.168.1.0/24"]

}

variable "priv_subnet_cidr" {
  default = ["192.168.2.0/24", "192.168.3.0/24"]

}

variable "pub_subnet_name" {
  type    = list(any)
  default = ["MySQL-pub-sub-01", "MySQL-pub-sub-02"]

}

variable "priv_subnet_name" {
  default = ["MySQL-priv-sub-01", "MySQL-priv-sub-02"]

}

variable "igw_name" {
  default = "MySQL-igw-01"

}

variable "nat_name" {
  default = "MySQL-nat-01"
}

variable "pub_route_table_name" {
  default = "MySQL-route-pub-01"
}

variable "priv_route_table_name" {
  default = "MySQL-route-priv-01"
}

variable "rt_cidr" {
  default = "0.0.0.0/0"
}

variable "ami_id" {
  default = "ami-04505e74c0741db8d"

}

variable "inst_type" {
  default = "t2.large"

}

variable "az" {
  default = ["us-east-1a", "us-east-1b"]

}

variable "host_name" {
  default = "bastion-host"
}

variable "node_name" {
  default = ["node-1", "node-2", "node-3"]

}

variable "pub_sg" {
  default = "bastion-sg"

}

variable "pub_descri" {
  default = "Security Group for bastion-host"
}

variable "protocol" {
  default = "tcp"

}

variable "priv_sg" {
  default = "node-sg"

}

variable "priv_descri" {
  default = "Security Group for node-hosts"

}

variable "region" {
  default = "us-east-1"

}

variable "key" {
  default = "useast1"

}