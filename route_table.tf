resource "aws_route_table" "MySQL_pub_route" {
  vpc_id = aws_vpc.MySQL_vpc.id

  route {
    cidr_block = var.rt_cidr
    gateway_id = aws_internet_gateway.MySQL_igw.id
  }

  tags = {
    Name = var.pub_route_table_name
  }
}

resource "aws_route_table" "MySQL_priv_route" {
  vpc_id = aws_vpc.MySQL_vpc.id

  route {
    cidr_block = var.rt_cidr
    gateway_id = aws_nat_gateway.MySQL_nat.id
  }

  tags = {
    Name = var.priv_route_table_name
  }
}

resource "aws_route_table_association" "pub_route" {
  count          = length(var.pub_subnet_cidr)
  subnet_id      = aws_subnet.MySQL_pub_subnet[count.index].id
  route_table_id = aws_route_table.MySQL_pub_route.id
}

resource "aws_route_table_association" "priv_route" {
  count          = length(var.priv_subnet_cidr)
  subnet_id      = aws_subnet.MySQL_priv_subnet[count.index].id
  route_table_id = aws_route_table.MySQL_priv_route.id
}
